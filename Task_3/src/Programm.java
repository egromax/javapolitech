import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Programm {

    public static void main(String[] args)
    {
        String[] array = {"1", "2", "3"};

        List<String> list =  toArrayList(array);

        list.add("4");

        System.out.print(list);
    }

    private static <T> List<T> toArrayList(T[] array) {
        List<T> arrayList = new ArrayList<>();

        Collections.addAll(arrayList, array);

        return arrayList;
    }
}

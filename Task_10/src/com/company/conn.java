package com.company;



import java.sql.*;



public class conn {
    public static Connection conn;
    public static Statement statmt;

    public static void Conn() throws ClassNotFoundException
    {
        try
        {
            conn = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/test_db" +
                    "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","Cjkzhbc[.ylfq857425");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    public static void CreateDB()
    {
        try
        {
            statmt = conn.createStatement();
            statmt.execute("DELETE FROM products");
            statmt.execute("ALTER TABLE products AUTO_INCREMENT = 1");
            statmt.execute("CREATE TABLE if not exists products (id INT NOT NULL , prodid INT NOT NULL PRIMARY KEY AUTO_INCREMENT, title VARCHAR(32) NOT NULL , cost INT NOT NULL );");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    public static void WriteDB(int Quantity) throws SQLException
    {

        String query = "INSERT INTO products (id, title, cost) VALUES (?, ?, ?); ";

        try(PreparedStatement p = conn.prepareStatement(query))
        {
            for (int i = 1; i <= Quantity; i++)
            {
                p.setInt(1, 1+ i*100);
                p.setString(2, "Laptop"+i);
                p.setInt(3, 55000 + i * 10000);
                p.execute();
            }
        }
    }

    public static void ReadDB(String name) throws SQLException
    {
        String query = "SELECT * FROM products";
        boolean flag = false;

        try(ResultSet resSet = statmt.executeQuery(query))
        {
            while(resSet.next())
            {
                String  title = resSet.getString("title");
                if(title.equals(name))
                {
                    int id = resSet.getInt("id");
                    int prodid = resSet.getInt("prodid");
                    int  cost = resSet.getInt("cost");
                    System.out.println( "ID = " + id );
                    System.out.println( "PRODID = " + prodid );
                    System.out.println( "TITLE = " + title );
                    System.out.println( "COST = " + cost );
                    System.out.println();
                    flag = true;
                }
            }
            if(!flag)
            {
                System.out.println("Товара" +" "+name+" "+ "нет в таблице");
            }
        }
    }

    public static void ChangeCost(String title, int newCost) throws SQLException
    {

        String query = "UPDATE products SET cost = ? WHERE title = ?;";
        try(PreparedStatement p = conn.prepareStatement(query))
        {
            p.setInt(1, newCost);
            p.setString(2,title);
            p.executeUpdate();
        }
    }

    public static void ReadDBInRange(int beginRange, int endRange) throws SQLException
    {
        String query = "SELECT * FROM products";
        boolean flag = false;

        try(ResultSet resSet = statmt.executeQuery(query))
        {
            while(resSet.next())
            {
                int  cost = resSet.getInt("cost");
                if(cost >= beginRange && cost <= endRange)
                {
                    int id = resSet.getInt("id");
                    int prodid = resSet.getInt("prodid");
                    String  title = resSet.getString("title");
                    System.out.println( "ID = " + id );
                    System.out.println( "PRODID = " + prodid );
                    System.out.println( "TITLE = " + title );
                    System.out.println( "COST = " + cost );
                    System.out.println();
                    flag = true;
                }
            }
            if(!flag)
            {
                System.out.println("Товаров в этом диапазоне нет");
            }
        }


    }

    public static void CloseDB() throws  SQLException
    {
        conn.close();
        statmt.close();
    }

}

package com.company;


import java.sql.SQLException;
import java.util.HashSet;
import java.util.Scanner;

public class db {


    public static void main(String[] args) throws SQLException {

        Scanner scn = new Scanner(System.in);
        boolean stop = false;
        HashSet<String> commands = new HashSet<String>();
        commands.add("/price");
        commands.add("/change_price");
        commands.add("/filter_by_price");
        commands.add("/exit");

        System.out.println("Введите количество записей");
        int N = scn.nextInt();
        System.out.println("Вводите сюда команды");

        try
        {
            conn.Conn();
            conn.CreateDB();
            conn.WriteDB(N);

            while (!stop)
            {
                String command = scn.next();
                if(!commands.contains(command))
                {
                    System.out.println("Kоманда" + " " +command+ " " + "не найдена");
                    scn.nextLine();
                    continue;
                }
                switch (command) {
                    case "/price":
                        String name = scn.next();
                        conn.ReadDB(name);
                        break;

                    case "/change_price":
                        String name2 = scn.next();
                        int newCost = scn.nextInt();
                        conn.ChangeCost(name2, newCost);
                        conn.ReadDB(name2);
                        break;

                    case "/filter_by_price":
                        int beginRange = scn.nextInt();
                        int endRange = scn.nextInt();
                        conn.ReadDBInRange(beginRange, endRange);
                        break;

                    case "/exit":
                        stop = true;
                        System.out.println("Завершение работы");
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            conn.CloseDB();
        }
    }
}

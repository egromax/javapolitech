package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner scn = new Scanner(System.in);
        int N = scn.nextInt();
        int[][] A = new int[N][N];

        for(int i = 0; i < N; i++)
        {
            for(int j = 0; j < N; j++)
            {
                if((j == i) || (j == N - (i+1)))
                    System.out.print(N);
                else
                    System.out.print(0);
            }
            System.out.println();
        }
    }
}

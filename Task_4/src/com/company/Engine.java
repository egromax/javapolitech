package com.company;

public class Engine implements EngineInterface{

    @Override
    public void startEngine() {
        System.out.println("The engine started");
    }

    @Override
    public void stopEngine() {
        System.out.println("The engine stop");
    }
}

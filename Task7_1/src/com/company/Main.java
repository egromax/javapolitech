package com.company;

import java.io.*;
import java.util.ArrayList;

public class Main {

    public static ArrayList<String> read(final String fileName) throws IOException {
        String l;
        FileInputStream fin = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        ArrayList<String> list = new ArrayList<String>();

        try
        {
            fin = new FileInputStream(fileName);
            isr = new InputStreamReader(fin);
            br = new BufferedReader(isr);

            while((l = br.readLine()) != null)
            {
                for (String word : l.split("[^\\w]+"))
                {
                    list.add(word);
                }
            }
        }
        catch(IOException e)
        {
            System.err.println(e);
        }
        finally
        {
            if (fin != null) fin.close();
            if (isr != null) isr.close();
            if (br != null) br.close();
            return list;
        }
    }

    public static void write(ArrayList<String> list) throws IOException {
        OutputStream fout = null;
        OutputStreamWriter wr = null;
        BufferedWriter out = null;

        try
        {
            fout = new FileOutputStream("Result.txt", false);
            wr = new OutputStreamWriter(fout);
            out = new BufferedWriter(wr);
            for (String l : list)
            {
                out.write(l);
                out.newLine();
            }
            out.write(list.size());
        }
        catch(IOException e)
        {
            System.err.println(e);
        }
        finally
        {
            if(out != null)
            {
                out.flush();
                out.close();
            }
            if(wr != null) wr.close();
            if(fout != null) fout.close();
        }
    }

    public static void main(String[] args) throws IOException {
        ArrayList<String> lexemes = read("lexemes.txt");
        ArrayList<String> list = read("JavaCode.txt");

        list.retainAll(lexemes);
        write(list);
        // write your code here
    }
}

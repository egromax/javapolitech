package com.company;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;

public class PropertiesApi
{
    private Properties property;
    private String filename;
    private static HashSet<String> files = new HashSet<String>();


    public PropertiesApi(String filename)
    {
        property = new Properties();
        this.filename = filename;
        readFile();
        outputValue();
    }


    private void readFile()
    {
        try(FileInputStream fis = new FileInputStream(filename))
        {
            if(!files.contains(filename))
            {
                property.load(fis);
                files.add(filename);
            }
            else
            {
                System.out.println("Файл уже использовался");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void outputValue()
    {
        for (String key : property.stringPropertyNames())
        {
            System.out.println(property.getProperty(key));
        }
    }
}

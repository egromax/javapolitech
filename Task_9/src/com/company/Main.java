package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Scanner scn = new Scanner(System.in);

        String firstFile = scn.nextLine();
        PropertiesApi pe1 = new PropertiesApi(firstFile + ".properties");

        String secondFile = scn.nextLine();
        PropertiesApi pe2 = new PropertiesApi(secondFile + ".properties");
    }
}

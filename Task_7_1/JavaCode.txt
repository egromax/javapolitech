package com.company;

import com.mysql.cj.log.Log;
import com.mysql.cj.util.TimeUtil;

import java.sql.*;
import java.util.Scanner;


public class conn {
    public static Connection conn;
    public static Statement statmt;
    public static ResultSet resSet;
    public static PreparedStatement p;

    public static void Conn() throws ClassNotFoundException, SQLException
    {
        try
        {
            conn = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/test_db" +
                    "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","Cjkzhbc[.ylfq857425");


        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    public static void CreateDB() throws ClassNotFoundException, SQLException
    {
        try
        {
            statmt = conn.createStatement();
            statmt.execute("DELETE FROM products");
            statmt.execute("ALTER TABLE products AUTO_INCREMENT = 1");
            statmt.execute("CREATE TABLE if not exists products (id INT NOT NULL , prodid INT NOT NULL PRIMARY KEY AUTO_INCREMENT, title VARCHAR(32) NOT NULL , cost INT NOT NULL );");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    public static void WriteDB(int Quantity) throws SQLException
    {

        String query = "INSERT INTO products (id, title, cost) VALUES (?, ?, ?); ";
        p = conn.prepareStatement(query);
        try
        {
            for (int i = 1; i <= Quantity; i++)
            {
                p.setInt(1, 1+ i*100);
                p.setString(2, "Laptop"+i);
                p.setInt(3, 55000 + i * 10000);
                p.execute();
            }
            p.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public static void ReadDB(String name) throws ClassNotFoundException, SQLException
    {
        try
        {
            resSet = statmt.executeQuery("SELECT * FROM products");

            while(resSet.next())
            {
                String  title = resSet.getString("title");
                if(title.equals(name))
                {
                    int id = resSet.getInt("id");
                    int prodid = resSet.getInt("prodid");
                    int  cost = resSet.getInt("cost");
                    System.out.println( "ID = " + id );
                    System.out.println( "PRODID = " + prodid );
                    System.out.println( "TITLE = " + title );
                    System.out.println( "COST = " + cost );
                    System.out.println();
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            resSet.close();
        }
    }

    public static void ChangeCost(String title, int newCost) throws SQLException {

        System.out.println("ewfewf");
        String query = "UPDATE products SET cost = ? WHERE title = ?;";
        try
        {
            //statmt.executeUpdate(query);
            PreparedStatement p = conn.prepareStatement(query);

            p.setInt(1, newCost);
            p.setString(2,title);
            p.executeUpdate();
            p.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    public static void CloseDB() throws ClassNotFoundException, SQLException
    {
        conn.close();
        statmt.close();
        //resSet.close();
    }

}

package com.company;

import java.io.*;
import java.util.ArrayList;

public class Main {

    public static ArrayList<String> read(final String fileName) throws IOException {
        String l;
        ArrayList<String> list = new ArrayList<String>();

        try(FileInputStream fin = new FileInputStream(fileName);
            InputStreamReader isr = new InputStreamReader(fin);
            BufferedReader br = new BufferedReader(isr);)
        {
            while((l = br.readLine()) != null)
            {
                for (String word : l.split("[^\\w]+"))
                {
                    list.add(word);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }

    public static void write(ArrayList<String> list) throws IOException {

        try(OutputStream fout = new FileOutputStream("Result.txt", false);
            OutputStreamWriter wr = new OutputStreamWriter(fout);
            BufferedWriter out = new BufferedWriter(wr))
        {

            for (String l : list)
            {
                out.write(l);
                out.newLine();
            }
            out.write(list.size() + "");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        ArrayList<String> lexemes = read("lexemes.txt");
        ArrayList<String> list = read("JavaCode.txt");

        list.retainAll(lexemes);
        write(list);
        // write your code here
    }
}

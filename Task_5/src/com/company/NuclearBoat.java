package com.company;

@Version(v=1.0f) //использование аннотации
public class NuclearBoat implements MovementInterface {

    private boolean boatInMotion;

    private NuclearBoatEngine engine;

    NuclearBoat()
    {
        boatInMotion = false;
        engine = new NuclearBoatEngine();
    }

    @Override
    public void move() {
        engine.startEngine();
    }

    @Override
    public void stop() {
        engine.stopEngine();
    }

    private class NuclearBoatEngine extends Engine
    {
        @Override
        public void startEngine() {
            boatInMotion = true;
            System.out.println("NuclearBoat engine started");
        }

        @Override
        public void stopEngine() {
            boatInMotion = false;
            System.out.println("NuclearBoat engine stop");
        }
    }

}

package com.company;

public interface MovementInterface {

    public void move();

    public void stop();

}

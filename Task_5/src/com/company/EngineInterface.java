package com.company;

public interface EngineInterface {

    void startEngine();

    void stopEngine();
}

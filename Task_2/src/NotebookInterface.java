public interface NotebookInterface {

    public void addNote(NotesInNotebook newNote);

    public void deleteNote(int number) throws IllegalAccessException;

    public void editNote(int number, String newNote) throws IllegalAccessException;

    public void displayAllNotes();
}

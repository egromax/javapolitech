public class NotesInNotebook {
    String note;

    NotesInNotebook(String note)
    {
        this.note = note;
    }

    public final String getNote() {
        return note;
    }

    public void setNote(String note){
        this.note = note;
    }
}

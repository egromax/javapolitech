import java.util.ArrayList;
import java.util.List;


public class Notebook implements NotebookInterface {

    private NotesInNotebook[] notes;
    private int length;
    private int count;

    Notebook() {
        length = 10;
        notes = new NotesInNotebook[length];
        count = 0;
    }


    public final int getNotesCount(){
        return count;
    }

    @Override
    public void addNote(NotesInNotebook newNote) {
        if (count == length -1)
        {
            growArray();
        }
        notes[count++] = newNote;
    }

    @Override
    public void deleteNote(int number) throws IllegalAccessException {
        if(number < 0 || number > count) throw new IllegalAccessException("Incorrect number");
        for(int i = number - 1; i < count - 1; i++)
            notes[i] = notes[i + 1];
        count--;
    }

    @Override
    public void editNote(int number, String newNote) throws IllegalAccessException {
        if(number < 0 || number > count) throw new IllegalAccessException("Incorrect number");
        notes[number-1].setNote(newNote);
    }

    @Override
    public void displayAllNotes() {
        for(int i = 0; i <= count-1 ; i++)
        {
            System.out.println(notes[i].getNote());
        }
    }

    private void growArray()
    {
        length = (length * 3) / 2 + 1;
        NotesInNotebook[] newArray = new NotesInNotebook[length];
        System.arraycopy(notes, 0, newArray, 0, count);
        notes = newArray;
        //length++;
    }

}

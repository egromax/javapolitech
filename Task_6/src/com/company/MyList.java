package com.company;

public class MyList<T> {
     //Внутренний класс
     class Node<T>
     {
     	Node<T> next;
     	T data;
     }


    // Dложенный класс
     /*static class Node<T>
    {
     	Node<T> next;
     	T data;
    }*/


    /*MyList()
    {
        //Локальный класс
        class Node<T>
        {
            Node<T> next;
            T data;
        }
    }*/
    Node<T> first;

    public void add(T data) {
        Node<T> node = new Node<T>();
        node.data = data;
        node.next = first;
        first = node;
    }

    @Override
    public String toString() {

        String s = "[";

        Node temp = first;

        while (temp != null) {
            s += temp.data;
            s += ", ";
            temp = temp.next;
        }

        return s + "]";
    }

}
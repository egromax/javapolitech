package com.company;

import java.io.*;
import java.util.ArrayList;


public class Main
{


    public static FileReader assign(String filename) throws IOException {

        FileReader fr = null;
        try
        {
            fr = new FileReader(filename);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            return fr;
        }
    }

    public static void write(ArrayList<String> list) throws IOException {

        try(FileWriter fw = new FileWriter("Result.txt"))
        {
            for(String s : list)
            {
                for(int i = 1; i < s.length(); i+=3)
                {
                    fw.write(s.charAt(i));
                }
                fw.write('\n');
            }
            fw.write(list.size() + "");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public static ArrayList<String> getLexemes() throws IOException {
        int b = 0;
        ArrayList list = new ArrayList();
        ArrayList<String> lexemes = new ArrayList<String>();

        try(FileReader fr = assign("Lexemes.txt"))
        {
            while ((b = fr.read()) != -1)
            {

                if (b != 10 && b != 13)
                {
                    list.add((char) b);
                }
                else
                {
                    if (!list.isEmpty())
                    {
                        lexemes.add(list.toString());
                        list.clear();
                    }
                }

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return lexemes;
    }

    public static void getLexemesFromJavaCode(ArrayList<String> result) throws IOException {
        int b = 0;
        int cout = 0;
        ArrayList<String> lexemes = getLexemes();
        ArrayList someLexemes = new ArrayList();


        try(FileReader r = assign("JavaCode.txt"))
        {
            while((b = r.read()) != -1)
            {
                if ((b != 10) && (b != 32) && (b != 46) && (b != 40) && (b != 91) && (b != 93) && (b != 58) && (b != 41) && (b != 59) && (b != 123) && (b != 125) && (b != 13))
                {
                    someLexemes.add((char) b);
                }
                else
                {
                    if (!someLexemes.isEmpty())
                    {
                        if (!lexemes.contains(someLexemes.toString()))
                        {
                            someLexemes.clear();
                        }
                        else
                        {
                            result.add(someLexemes.toString());
                            someLexemes.clear();
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws IOException {

        ArrayList<String> result = new ArrayList<String>();
        getLexemesFromJavaCode(result);
        write(result);
    }
}
